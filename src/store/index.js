import Vue from 'vue'
import Vuex from 'vuex'

import { clearToken } from '../components/mixin/auth'
import { Cookies } from 'quasar'

const token = Cookies.get('ageeyeToken')
const user = Cookies.get('ageeyeUser')

if (user) {
  user.token = 'ageeye ' + token
}

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      windowWidth: 0,
      searchKey: '',
      scrollStatus: {},
      history: [],
      pageMapData: null,
      auth: user || null,
      unreadNotifyCount: 0
    },
    mutations: {
      setWindowSize (state, size) {
        state.windowWidth = size.width
      },
      /**
       *
       * @param {Object} state
       * @param {Object} user
       */
      setAuth (state, user) {
        state.auth = Object.assign({}, state.auth, user)
      },
      setScrollStatus (state, status) {
        state.scrollStatus = status || {}
      },
      setUnreadNotifyCount (state, count) {
        state.unreadNotifyCount = count
      },
      setPageMapData (state, data) {
        state.pageMapData = Object.freeze(data)
      },
      /**
       *
       * @param {Object} state
       * @param {string} history
       */
      pushHistory (state, history) {
        if (state.history[0] !== history) {
          state.history.unshift(history)
        }
        if (state.history.length > 50) {
          state.history.length = 50
        }
      },
      quit (state) {
        state.auth = null
        clearToken()
      }
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
