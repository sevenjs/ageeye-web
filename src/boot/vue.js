import Common from '../components/index'
import {
  getQuerySet,
  getRequest,
  postRequest,
  patchRequest,
  putRequest,
  getRoute,
  deleteRequest
} from '../utils/request'

import { timesince } from '../utils/time'
import { mapState } from 'vuex'
import '../css/iconfont.css'
import '../css/HYJinKaiJ-embed.css'

export default async ({ app, Vue }) => {
  Vue.use(Common)
  Vue.prototype.$bus = new Vue()
  Vue.mixin({
    computed: {
      ...mapState([
        'auth'
      ])
    },
    methods: {
      getQuerySet,
      getRequest,
      postRequest,
      deleteRequest,
      patchRequest,
      putRequest,
      getRoute,
      timesince,
      notify (type, message) {
        let color

        if (type === 'success') {
          color = 'positive'
        } else if (type === 'error') {
          color = 'red'
        } else if (type === 'info') {
          color = 'brown'
        }

        this.$q.notify({
          color,
          message
        })
      },
      confirm ({ title, message, ok = '确定', cancel = '取消' }) {
        return new Promise((resolve, reject) => {
          this.$q.dialog({
            title,
            message,
            ok,
            cancel,
            persistent: true
          }).onOk(() => {
            resolve(true)
          }).onCancel(() => {
            resolve(false)
          })
        })
      },
      prompt ({ title, message, data = '', type = 'text', ok = '确定', cancel = '取消' }) {
        return new Promise((resolve, reject) => {
          this.$q.dialog({
            title,
            message,
            prompt: {
              model: 'moren',
              type
            },
            ok,
            cancel
          }).onOk((data) => {
            resolve(data)
          }).onCancel(() => {
            resolve(false)
          })
        })
      },
      print () {
        console.log('===========')
      }
    }
  })
}
