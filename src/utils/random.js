/**
 * Created by CaoJiang on 2017/3/30.
 */

export function randomStr (isNumber = true) {
  let str = isNumber ? 'abcdefghijklmnoqprstvuwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' : 'abcdefghijklmnoqprstvuwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
  return str[parseInt(Math.random() * str.length)]
}

export function randomInt (n, m) {
  return Math.floor(Math.random() * (m - n + 1) + n)
}

export function randomFloat (n, m) {
  return Math.random() * (m - n + 1) + n
}

export function uuid () {
  let d = new Date().getTime()
  const uuid = 'xxxxxxxx'.replace(/[xy]/g, c => {
    const r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
  })
  return new Date().format('yyyyMMddhhmmss') + uuid
}
