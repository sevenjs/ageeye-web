/**
 *
 * @param {Element} element
 */
export function scrollToElement (element) {
  const offsetTop = element.offsetTop
  document.documentElement.scrollTop = offsetTop
}
