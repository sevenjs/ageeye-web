/**
 * Created by CL on 2017/12/21.
 */

export default {
  title: 'standard',
  Point: {
    'default': {
      shadow: 'auto',
      marker: 'il-tr',
      textFill: '#000000',
      textWeight: 'bold',
      textName: '{name}',
      textOpacity: 1,
      textHaloOpacity: 1,
      textHaloFill: 0
    }
  },
  LineString: {
    'default': {
      lineColor: '#1E90FF',
      lineWidth: 2,
      lineOpacity: 1,
      dashArray: 'solid'
    }
  },
  Polygon: {
    'default': {
      lineColor: '#1E90FF',
      lineWidth: 2,
      lineOpacity: 1,
      polygonOpacity: 0.5,
      pattern: 'none'
    }
  }
}
