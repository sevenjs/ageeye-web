/**
 * Created by CL on 2017/3/19.
 */
import Vue from 'vue'

import Loading from './loading.vue'
import Head from './head.vue'
import Heads from './heads.vue'
import SearchUser from './searchUser.vue'
import Search from './search.vue'
import CommentBox from './commentBox.vue'
import CommentBoxes from './commentBoxes.vue'
import MapImageBoxes from './mapImageBoxes.vue'
import TextInput from './textInput.vue'
import UserIndex from './userIndex.vue'
import Dialog from './dialog.vue'
import None from './none.vue'
import ForumList from './forumList.vue'
import Page from './page.vue'
import UserList from './userList.vue'
import Pagination from './pagination.vue'
import Order from './order.vue'
import Notifications from './notifications.vue'
import Oauth from './oauth.vue'
import Up from './up.vue'
import Collect from './collect.vue'
import Share from './share.vue'
import ShareDialog from './shareDialog.vue'
import Notify from './notify.vue'
import Upload from './upload.vue'
import Blog from './blog.vue'
import Table from './table.vue'
import PopoverMenu from './popoverMenu.vue'

const components = {
  Loading,
  Head,
  Heads,
  SearchUser,
  Search,
  CommentBox,
  CommentBoxes,
  MapImageBoxes,
  TextInput,
  UserIndex,
  Dialog,
  None,
  ForumList,
  Page,
  Pagination,
  UserList,
  Order,
  Notifications,
  Oauth,
  Up,
  Collect,
  Share,
  ShareDialog,
  Notify,
  Upload,
  Blog,
  Table,
  PopoverMenu
}

const install = function () {
  Object.keys(components).forEach((key) => {
    Vue.component(components[key].name, components[key])
  })
}

export default {
  install
}
