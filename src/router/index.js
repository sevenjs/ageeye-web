import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    const nextRoute = [ 'account' ]
    const store = Router.app.$store
    const auth = store.state.auth
    const path = to.path

    store.commit('pushHistory', path)

    if (!auth) {
      nextRoute.forEach(route => {
        if (path.includes(route) === true) {
          next({ path: '/login' })
        }
      })
    } else if (path === '/login' || path === '/register') {
      next({ path: '/account' })
    }

    next()
  })

  return Router
}
