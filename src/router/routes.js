
const routes = [
  {
    path: '',
    component: () => {
      let isJtsTopic = false

      if (process.env.NODE_ENV === 'development') {
        if (location.hostname === '127.0.0.1') {
          isJtsTopic = true
        }
      } else {
        const match = location.hostname.match(/(\S+).ageeye.cn/)
        if (match[1] === 'jts') {
          isJtsTopic = true
        }
      }

      return isJtsTopic ? import('pages/topics/jts') : import('layouts/default')
    },
    // redirect: 'account/map/',
    children: [
      {
        path: '',
        component: () => import('pages/index')
      },
      {
        path: 'login',
        component: () => import('pages/login')
      },
      {
        path: 'register',
        component: () => import('pages/register')
      },
      {
        path: 'reset',
        component: () => import('pages/reset')
      },
      {
        path: 'account',
        component: () => import('pages/account'),
        redirect: to => {
          return 'account/maps'
        },
        children: [
          {
            path: 'maps',
            components: {
              default: () => import('pages/account/maps'),
              nav: () => import('pages/account/nav/map')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'maps' }),
              nav: (route) => ({ ...route.query, type: 'maps' })
            }
          },
          {
            path: 'share',
            components: {
              default: () => import('pages/account/maps'),
              nav: () => import('pages/account/nav/map')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'share' }),
              nav: (route) => ({ ...route.query, type: 'share' })
            }
          },
          {
            path: 'favorites',
            component: () => import('pages/account/favorites'),
            props: (route) => ({ ...route.query })
          },
          {
            path: 'forums',
            component: () => import('pages/account/forums')
          },
          {
            path: 'people',
            component: () => import('components/userIndex'),
            redirect: 'people/map',
            children: [
              {
                path: 'map',
                component: () => import('components/userIndex/article'),
                props: (route) => ({ ...route.query, type: 'map' })
              },
              {
                path: 'share',
                component: () => import('components/userIndex/article'),
                props: (route) => ({ ...route.query, type: 'share' })
              },
              {
                path: 'forum',
                component: () => import('components/userIndex/article'),
                props: (route) => ({ ...route.query, type: 'forum' })
              },
              {
                path: 'follower',
                component: () => import('components/userIndex/follow'),
                props: (route) => ({ ...route.query, type: 'follower' })
              },
              {
                path: 'followed',
                component: () => import('components/userIndex/follow'),
                props: (route) => ({ ...route.query, type: 'followed' })
              }
            ]
          },
          {
            path: 'notifications',
            components: {
              default: () => import('pages/account/notifications'),
              nav: () => import('pages/account/nav/notifications')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'all' })
            }
          },
          {
            path: 'notifications/activity',
            components: {
              default: () => import('pages/account/notifications'),
              nav: () => import('pages/account/nav/notifications')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'activity' })
            }
          },
          {
            path: 'notifications/comment',
            components: {
              default: () => import('pages/account/notifications'),
              nav: () => import('pages/account/nav/notifications')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'comment' })
            }
          },
          {
            path: 'notifications/follow',
            component: () => import('pages/account/notifications'),
            components: {
              default: () => import('pages/account/notifications'),
              nav: () => import('pages/account/nav/notifications')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'follow' })
            }
          },
          {
            path: 'notifications/letter',
            component: () => import('pages/account/notifications'),
            components: {
              default: () => import('pages/account/notifications'),
              nav: () => import('pages/account/nav/notifications')
            },
            props: {
              default: (route) => ({ ...route.query, type: 'letter' })
            }
          },
          {
            path: 'settings',
            components: {
              default: () => import('pages/account/settings'),
              nav: () => import('pages/account/nav/settings')
            },
            redirect: 'settings/name',
            children: [
              {
                path: 'safe',
                component: () => import('pages/account/settings/safe')
              },
              {
                path: 'head',
                component: () => import('pages/account/settings/head')
              },
              {
                path: 'name',
                component: () => import('pages/account/settings/name')
              }
            ]
          },
          {
            path: 'oauth/:id',
            redirect: '/'
          }
        ]
      },
      {
        path: 'account/nav',
        component: () => import('pages/account/listNav')
      },
      {
        path: 'map',
        component: () => import('pages/maps'),
        props: (route) => ({ ...route.query })
      },
      {
        path: 'tag/:keyword',
        component: () => import('pages/maps'),
        props: (route) => ({ ...route.query, ...route.params })
      },
      {
        path: 'map/:id',
        component: () => import('pages/map'),
        props: (route) => ({ ...route.params })
      },
      {
        path: 'topic',
        component: () => import('pages/topics')
      },
      {
        path: 'topic/:attr',
        component: () => import('pages/topic'),
        props: (route) => ({ ...route.params, ...route.query, createdQuery: false })
      },
      {
        path: 'forum',
        component: () => import('pages/forums'),
        props: (route) => ({ ...route.query, type: 'all' })
      },
      {
        path: 'forum/forum',
        component: () => import('pages/forums'),
        props: (route) => ({ ...route.query, type: 'forum' })
      },
      {
        path: 'forum/feedback',
        component: () => import('pages/forums'),
        props: (route) => ({ ...route.query, type: 'feedback' })
      },
      {
        path: 'forum/notice',
        component: () => import('pages/forums'),
        props: (route) => ({ ...route.query, type: 'notice' })
      },
      {
        path: 'forum/chat',
        component: () => import('pages/forums'),
        props: (route) => ({ ...route.query, type: 'chat' })
      },
      {
        path: 'forum/:id',
        component: () => import('pages/forum'),
        props: (route) => ({ ...route.params })
      },
      {
        path: 'people/:id',
        component: () => import('pages/people'),
        redirect: 'people/:id/map',
        props: (route) => ({ ...route.query, ...route.params, type: 'map' }),
        children: [
          {
            path: 'map',
            component: () => import('components/userIndex/article'),
            props: (route) => ({ ...route.query, ...route.params, type: 'map' })
          },
          {
            path: 'share',
            component: () => import('components/userIndex/article'),
            props: (route) => ({ ...route.query, ...route.params, type: 'share' })
          },
          {
            path: 'forum',
            component: () => import('components/userIndex/article'),
            props: (route) => ({ ...route.query, ...route.params, type: 'forum' })
          },
          {
            path: 'follower',
            component: () => import('components/userIndex/follow'),
            props: (route) => ({ ...route.query, ...route.params, type: 'follower' })
          },
          {
            path: 'followed',
            component: () => import('components/userIndex/follow'),
            props: (route) => ({ ...route.query, ...route.params, type: 'followed' })
          }
        ]
      },
      {
        path: 'search/',
        component: () => import('pages/search'),
        children: [
          {
            path: 'map/:keyword',
            component: () => import('pages/search/map'),
            props: (route) => ({ ...route.query, ...route.params })
          },
          {
            path: 'forum/:keyword',
            component: () => import('pages/search/forum'),
            props: (route) => ({ ...route.query, ...route.params })
          },
          {
            path: 'user/:keyword',
            component: () => import('pages/search/user'),
            props: (route) => ({ ...route.query, ...route.params })
          }
        ]
      },
      {
        path: 'china/',
        component: () => import('pages/china'),
        props: (route) => ({ attr: 'china', ...route.query })
      },
      {
        path: 'about/',
        component: () => import('components/blog'),
        props: (route) => ({ id: 12 })
      },
      {
        path: 'help/',
        component: () => import('components/blog'),
        props: (route) => ({ id: 1 })
      },
      {
        path: 'donation/',
        component: () => import('components/blog'),
        props: (route) => ({ id: 6 })
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/404.vue')
  })
}

export default routes
